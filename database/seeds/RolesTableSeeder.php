<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'Admin']);
        $role->syncPermissions( Permission::all()); // Give Admin Role all permissions
        
        $role = Role::create(['name' => 'Stock Manager']);
        $role->givePermissionTo(Permission::findByName('view entities'));
        $role->givePermissionTo(Permission::findByName('create entity'));
        $role->givePermissionTo(Permission::findByName('edit entity'));
        $role->givePermissionTo(Permission::findByName('delete entity'));

        Role::create(['name' => 'Secretary']);


        Role::create(['name' => 'User']);


    }
}