<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;


class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // General
        Permission::create(['name' => 'view management']);
        Permission::create(['name' => 'view users']);
        Permission::create(['name' => 'view categories']);
        Permission::create(['name' => 'view roles']);
        Permission::create(['name' => 'view entities']);

        // Users
        Permission::create(['name' => 'create user']);
        Permission::create(['name' => 'edit user']);
        Permission::create(['name' => 'delete user']);
        Permission::create(['name' => 'block user']);
        Permission::create(['name' => 'approve user']);
        Permission::create(['name' => 'reset password']);

        // Roles
        Permission::create(['name' => 'view role']);
        Permission::create(['name' => 'create role']);
        Permission::create(['name' => 'edit role']);
        Permission::create(['name' => 'delete role']);

        // Model
        Permission::create(['name' => 'view entity']);
        Permission::create(['name' => 'create entity']);
        Permission::create(['name' => 'edit entity']);
        Permission::create(['name' => 'delete entity']);

        // Categories
        Permission::create(['name' => 'view category']);
        Permission::create(['name' => 'create category']);
        Permission::create(['name' => 'edit category']);
        Permission::create(['name' => 'delete category']);
    }
}
