<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->first_name = "John";
        $user->last_name = "Doe";
        $user->username = "jodoe015";
        $user->email = "john.doe@gmail.com";
        $user->password = bcrypt("john");
        $user->is_approved = true;
        $user->save();
        $user->assignRole('Admin');

        $user = new User();
        $user->first_name = "Jane";
        $user->last_name = "Doe";
        $user->username = "jadoe127";
        $user->email = "jane.doe@gmail.com";
        $user->password = bcrypt("jane");
        $user->is_approved = true;
        $user->save();
        $user->assignRole('Stock Manager');

        $user = new User();
        $user->first_name = "Max";
        $user->last_name = "Mustermann";
        $user->username = "mamus753";
        $user->email = "max.muster@gmail.com";
        $user->password = bcrypt("doe");
        $user->save();
        $user->assignRole('Secretary');
    }
}
