# Reservation System

## Introduction

This is a BTSi project made by Yvan Martins Lourenço. It's a reservation and stocking system, where user can book objects or rooms. It also outputs a php made image to display on a external monitor, which can be mounted next to the room which also displays the reservations date for that room. The image also includes a QR which send the URL for a more detailed view.

## Requirements

- Ubuntu 16.04.3 LTS
- PHP 7.2.2
- MySQL 14.14
- Composer 1.6.3+

## Installation

1. Install all the requirements before cloning the project.
2. Then run composer to download and install all the dependecies 
3. Create you `.env` file by copying the template from the `.env.example` file.
4. Config the `.env` file.
5. Now create the database with the command `php artisan migrate --seed`. This will create the database for the project and include some default users. (They can be deleted later)
6. And you're ready to go.

