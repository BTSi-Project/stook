<div id="deleteModal" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title">Delete Role</h1>
            </div>

            <div class="modal-body">
                <p>Are you sure want to permanently delete this role? You will lose all permissions and user configuration.</p>
                <form method="POST" action="{{ route('roles.destroy',$role) }}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>