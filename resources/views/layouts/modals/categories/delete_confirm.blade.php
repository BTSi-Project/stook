<div id="deleteModal" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title">Delete Category</h1>
            </div>

            <div class="modal-body">
                <p>Are you sure want to permanently delete this category? You will lose each entity created inside this catergory.</p>
                <form method="POST" action="{{ route('categories.destroy',$category) }}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>