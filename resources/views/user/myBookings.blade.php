@extends('adminlte::page')

@section('title', 'Stook - My Bookings')

@section('content_header')
    <h1>My Bookings</h1>{{ Breadcrumbs::render('myBookings') }}
@stop
@section('content')

    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Current Bookings</h3>
                </div>

                <table class="table">
                    @if(count($currentBookings))
                        <tr>
                            <th>Entity</th>
                            <th>Category</th>
                            <th>From</th>
                            <th>To</th>
                        </tr>
                        @foreach($currentBookings as $booking)
                            <tr>
                                <td>{{ $booking->entity->name }}</td>
                                <td>{{ $booking->entity->category->name }}</td>
                                <td>{{ $booking->from }}</td>
                                <td>{{ $booking->to }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>There are no categories yet.</td>
                        </tr>
                    @endif
                </table>
                <div class="box-footer">
                    <div class="col-sm-5">
                        <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
                            Showing {{ $currentBookings->count() }} of {{ $currentBookings->total() }} entries
                        </div>
                    </div>
                    <div class="col-sm-7">
                        {{ $currentBookings->appends(array_except(Request::query(), 'page'))->links('pagination.default') }}

                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Booking History</h3>
                </div>

                <table class="table">
                    @if(count($pastBookings))
                        <tr>
                            <th>Entity</th>
                            <th>Category</th>
                            <th>From</th>
                            <th>To</th>
                        </tr>
                        @foreach($pastBookings as $booking)
                            <tr>
                                <td>{{ $booking->entity->name }}</td>
                                <td>{{ $booking->entity->category->name }}</td>
                                <td>{{ $booking->from }}</td>
                                <td>{{ $booking->to }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>There are no categories yet.</td>
                        </tr>
                    @endif
                </table>
                <div class="box-footer">
                    <div class="col-sm-5">
                        <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
                            Showing {{ $pastBookings->count() }} of {{ $pastBookings->total() }} entries
                        </div>
                    </div>
                    <div class="col-sm-7">
                        {{ $pastBookings->appends(array_except(Request::query(), 'history_page'))->links('pagination.default') }}
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
