@extends('adminlte::page')
@section('title', 'Stook - Categories Management')

@section('content_header')
    <h1>Categories Management</h1>
@stop
@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="box box-info">
                <table class="table">
                    @if(count($categories))
                        <tr>
                            <th>Name</th>
                            <th>Entities</th>
                        </tr>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->name }}</td>
                                <td>{{ count($category->entities) }}</td>
                                @if(Auth::user()->hasPermissionTo('edit category'))
                                    <td><a class="btn btn-primary btn-sm pull-right"
                                           href="{{ route('categories.edit',$category->id) }}"><i
                                                    class="fa fa-cog"></i></a></td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>There are no categories yet.</td>
                        </tr>
                    @endif
                    @if(Auth::user()->hasPermissionTo('create category'))
                        <tr>
                            <form action="{{ route('categories.store') }}" method="POST">
                                @csrf
                                <td colspan="2">
                                    <input type="text" class="form-control" name="name" placeholder="Category name" autofocus>
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-success btn-block pull-right">Add</button>
                                </td>
                            </form>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
@stop