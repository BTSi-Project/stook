@extends('adminlte::page')
@section('title', 'Stook - Categories Management')

@section('content_header')
    <h1>Categories Management</h1>
@stop
@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Entities</h3>
                </div>
                <div class="box-body ">
                    <table class="table">
                        @if(count($category->entities))
                            @foreach($category->entities as $entity)
                                <tr>
                                    <td>{{$entity->name}}</td>
                                    @if(Auth::user()->hasPermissionTo('delete entity'))
                                        <td>
                                            <form action="{{ route('entities.destroy',$entity) }}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <input type="hidden" name="id" value="{{$entity->id}}">
                                                <input type="hidden" name="category_id" value="{{ $category->id }}">
                                                <button type="submit" class="btn btn-danger btn-sm pull-right"><i
                                                            class="fa fa-ban"></i></button>
                                            </form>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        @else
                            <p>This role has no entities.</p>
                        @endif
                        @if(Auth::user()->hasPermissionTo('create entity'))
                            <form action="{{ route('entities.store',$category) }}" method="POST"
                                  class="form-inline">
                                @csrf
                                <tr>
                                    <td colspan="">
                                        <input type="text" name="name" class="form-control" placeholder="Name" autofocus>
                                        <input type="hidden" name="category_id" value="{{ $category->id }}">

                                    </td>
                                    <td>
                                        <button type="submit" class="btn btn-success btn-block pull-right">
                                            Add
                                        </button>
                                    </td>
                                </tr>
                            </form>
                        @endif
                    </table>
                </div>
            </div>
        </div>

    </div>
    @if(Auth::user()->hasPermissionTo('delete category'))
        <div class="row">
            <div class="col-xs-12">
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                    Delete Category
                </button>
            </div>
        </div>
        @include('layouts.modals.categories.delete_confirm')
    @endif

@stop