@extends('adminlte::page')
@section('title', 'Stook - User Manangement')

@section('content_header')
    <h1>Edit User</h1>
@stop
@section('content')
    <div class="box box-primary">

        {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
        <div class="box-body">
            {{ csrf_field() }}
            <div class="form-group">
                {{ Form::label('username', 'Username') }}
                {{ Form::text('username', $user->username, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('first_name', 'First name') }}
                {{ Form::text('first_name', $user->first_name, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('last_name', 'Last name') }}
                {{ Form::text('last_name', $user->last_name, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('email', 'Email') }}
                {{ Form::text('email', $user->email, array('class' => 'form-control')) }}
            </div>

            <div class="box-footer">
                {{ Form::button('<i class="fa fa-edit"></i> Submit changes',['type' => 'submit', 'class' => 'btn btn-primary bts-sm']) }}
            </div>
        </div>
        {!! Form::close() !!}

    </div>
@stop