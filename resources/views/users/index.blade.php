@extends('adminlte::page')

@section('title', 'Stook - User Manangement')

@section('content_header')
    <h1>User Management
        @if(Auth::user()->hasPermissionTo('create user'))
            <a href="{{ route('users.create') }}" class="btn btn-success pull-right">Add User</a>
        @endif
    </h1>
@stop
@section('content')
    <div class="row">
        @if(count($approved_users))
            @include('users.layout.approved_users')
        @endif
        @if(count($pending_users))
            @include('users.layout.pending_users')
        @endif
        @if(count($blocked_users))
            @include('users.layout.blocked_users')
        @endif
    </div>
@stop
