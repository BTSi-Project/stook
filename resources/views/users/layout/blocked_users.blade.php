<div class="col-md-4">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Blocked users</h3>
        </div>
        <div class="box-body ">
            <table class="table table-bordered">
                <tr>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Username</th>
                    <th style="width: 20px"></th>
                </tr>
                @foreach($blocked_users as $user)
                    <tr>
                        <td>{{$user->first_name}}</td>
                        <td>{{$user->last_name}}</td>
                        <td>{{$user->username}}</td>
                        <td>
                            <div class="btn-group">
                                {{-- User has block permissions --}}
                                @if(Auth::user()->username != $user->username)
                                    @if(Auth::user()->hasPermissionTo("block user"))
                                        {{-- Restore --}}
                                        <form action="{{ route('users.restore',$user->id) }}" method="POST">
                                            @method('PATCH')
                                            @csrf
                                            <button type="submit" class="btn btn-success btn-sm btn-block">Unblock</button>
                                        </form>

                                    @endif
                                    @if(Auth::user()->hasPermissionTo("delete user"))
                                        {{-- Permanent Delete --}}
                                            <form action="{{ route('users.fullDelete',$user->id) }}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-danger btn-sm btn-block" style="margin-top: 4px">Delete</button>
                                            </form>
                                    @endif
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>