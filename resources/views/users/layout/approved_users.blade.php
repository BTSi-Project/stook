<div class="col-md-4">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Approved users</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body ">
            <table class="table table-bordered">
                <tr>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Username</th>
                    <th style="width: 40px"></th>
                </tr>
                @foreach($approved_users as $user)
                    <tr>
                        <td>{{$user->first_name}}</td>
                        <td>{{$user->last_name}}</td>
                        <td>{{$user->username}}</td>
                        <td>

                            @if(Auth::user()->hasPermissionTo("edit user"))
                                <a class="btn btn-info btn-sm btn-block"
                                   href="{{ route('users.edit',$user->id) }}">Edit</a>
                            @endif

                            @if(Auth::user()->hasPermissionTo("block user") && Auth::user()->username != $user->username)
                                <form action="{{ route('users.destroy', $user->id) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-warning btn-sm btn-block btn"
                                            style="margin-top: 4px">Block
                                    </button>
                                </form>
                            @else
                                <button type="submit" class="btn btn-warning btn-sm btn-block btn"
                                        style="margin-top: 4px" disabled data-toggle="tooltip" data-placement="top" title="You can't block yourself">Block
                                </button>
                            @endif


                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>