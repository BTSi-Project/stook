<div class="col-md-4">
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Pending users</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body ">
            <table class="table table-bordered">
                <tr>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Username</th>
                    <th style="width: 20px"></th>
                </tr>
                @foreach($pending_users as $user)
                    @if(empty($user->deleted_at))
                        <tr>
                            <td>{{$user->first_name}}</td>
                            <td>{{$user->last_name}}</td>
                            <td>{{$user->username}}</td>
                            <td>
                                @if(Auth::user()->hasPermissionTo('approve user'))
                                    <form action="{{ route('users.approve',$user->id) }}" method="POST"
                                          class="form-inline">
                                        @method('PATCH')
                                        @csrf
                                        <button type="submit" class="btn btn-success btn-sm"><i
                                                    class="fa fa-check form-inline fa-fw"></i></button>
                                    </form>

                                    <form action="{{ route('users.destroy',$user->id) }}" method="POST"
                                          class="form-inline">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger btn-sm" style="margin-top: 4px"><i
                                                    class="fa fa-times fa-fw"></i></button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
            </table>
        </div>
    </div>
</div>