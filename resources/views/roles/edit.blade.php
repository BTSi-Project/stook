@extends('adminlte::page')
@section('title', 'Stook - Role Manangement')

@section('content_header')
    <h1>Edit Role: {{$role->name}}</h1>
@stop
@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Permissions</h3>
                </div>
                <div class="box-body ">
                    <table class="table">
                        @if(count($role->permissions))
                            @foreach($role->permissions as $permission)
                                <tr>
                                    <td>{{$permission->name}}</td>
                                    <td>
                                        <form action="{{ route('roles.remove',$role) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <input type="hidden" name="id" value="{{$permission->id}}">
                                            <input type="hidden" name="type" value="permission">
                                            <button type="submit" class="btn btn-danger btn-sm pull-right"><i
                                                        class="fa fa-ban"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <p>This role has no permissions.</p>
                        @endif
                        @if(count($permissions_not_acquired))
                            <form action="{{ route('roles.add',$role) }}" method="POST"
                                  class="">
                                @method('POST')
                                @csrf
                                <tr>
                                    <td>
                                        <select name="id" class="form-control">
                                            @foreach($permissions_not_acquired as $permission)
                                                <option value="{{$permission->id}}">{{$permission->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input type="hidden" name="type" value="permission">
                                        <button type="submit" class="btn btn-success btn-block pull-right">
                                            Add
                                        </button>
                                    </td>
                                </tr>
                            </form>

                        @endif
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Users</h3>
                </div>
                <div class="box-body ">
                    <table class="table">
                        @if(count($role->users))
                            @foreach($role->users as $user)
                                <tr>
                                    <td>{{$user->first_name . " ". $user->last_name}}</td>
                                    <td>{{$user->username}}</td>
                                    <td>
                                        <form action="{{ route('roles.remove',$role) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $user->id }}">
                                            <input type="hidden" name="type" value="user">
                                            <button type="submit" class="btn btn-danger btn-sm pull-right"><i
                                                        class="fa fa-ban"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <p>No user in this role.</p>
                        @endif
                        @if(count($users_not_in_role))
                            <form action="{{ route('roles.add',$role) }}" method="POST"
                                  class="form-inline">
                                @method('POST')
                                @csrf
                                <tr>
                                    <td colspan="2">
                                        <input type="hidden" name="type" value="user">
                                        <select name="id" id="" class="form-control">
                                            @foreach($users_not_in_role as $user)
                                                <option value="{{ $user->id }}">{{ $user->first_name . " " . $user->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <button type="submit" class="btn btn-success btn-block pull-right">
                                            Add
                                        </button>
                                    </td>
                                </tr>
                            </form>
                        @endif
                    </table>
                </div>
            </div>
        </div>

    </div>
    @if(Auth::user()->hasPermissionTo('delete role'))
        <div class="row">
            <div class="col-xs-12">
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                    Delete Role
                </button>
            </div>
        </div>
        @include('layouts.modals.roles.delete_confirm')
    @endif
@stop
