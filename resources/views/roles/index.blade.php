@extends('adminlte::page')
@section('title', 'Stook - Role Manangement')

@section('content_header')
    <h1>Role Management</h1>
@stop
@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="box box-info">
                <table class="table">
                    <tr>
                        <th>Name</th>
                        <th>Users</th>
                        <th>Permission</th>
                    </tr>
                    @foreach($roles as $role)
                        <tr>
                            <td>{{ $role->name }}</td>
                            <td>{{ count($role->users) }}</td>
                            <td>{{ count($role->permissions) }}</td>
                            @if(Auth::user()->hasPermissionTo('edit role'))
                                <td><a class="btn btn-primary btn-sm pull-right"
                                       href="{{ route('roles.edit',$role->id) }}"><i
                                                class="fa fa-cog"></i></a></td>
                            @endif
                        </tr>
                    @endforeach
                    @if(Auth::user()->hasPermissionTo('create role'))
                        <tr>
                            <form action="{{ route('roles.store') }}" method="POST">
                                @csrf
                                <td colspan="3">
                                    <input type="text" class="form-control" name="role-name" placeholder="Role name">
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-success btn-block pull-right">Add</button>
                                </td>
                            </form>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
@stop