@extends('adminlte::page')

@section('title', 'Stook - Dashboard')

@section('content_header')
    <h1>Dashboard</h1>{{ Breadcrumbs::render('dashboard') }}
@stop
@section('content')
    <div class="row">
        @foreach($categories as $category)
            <div class="col-xs-12 col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $category->name }}</h3>


                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table">
                            @foreach($category->entities as $entity)

                                <tr>
                                    <td style="vertical-align: middle">{{ $entity->name }}</td>
                                    @if($entity->status())
                                        <td style="vertical-align: middle"><span style="padding: 5px" class="label label-success pull-right">Available</span></td>
                                    @else
                                        <td style="vertical-align: middle"><span style="padding: 5px" class="label label-danger pull-right">Unavailable</span></td>
                                    @endif
                                    <td style="width: 10px"><a href="{{ route('bookings.create',$entity->id) }}"
                                           class="btn btn-sm btn-primary pull-right">Bookings</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        @endforeach
@stop
