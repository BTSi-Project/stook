@extends('adminlte::page')
@section('title', 'Stook - Categories Management')
@section('css')
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css"/>
@stop
@section('content_header')
    <h1>{{ $entity->name }}</h1>
@stop
@section('content')
    <div class="row">
        <div class="col-md-8">
            @if(count($bookings))
                @foreach($bookings as $booking)
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ $booking->user->first_name . " ". $booking->user->last_name }}</h3>
                            <p>{{ $booking->comment }}</p>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-4">
                                    <b>From:</b> {{ date("d-m-Y",strtotime($booking->from)) ." ". date("H:i",strtotime($booking->from)) }}
                                </div>
                                <div class="col-xs-4">
                                    <b>To:</b> {{ date("d-m-Y",strtotime($booking->from)) ." ". date("H:i",strtotime($booking->to)) }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <p>There are no bookings yet.</p>
            @endif
        </div>
        @if(Auth::check())
            <div class="col-xs-12 col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Book</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{ route('bookings.store',$entity) }}" method="POST">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="daterange">Date range</label>
                                <input type='text' class="form-control" name="daterange" readonly/>
                            </div>
                            <div class="form-group">
                                <label for="comment">Comment</label>
                                <textarea type="text" class="form-control" name="comment"
                                          placeholder="Comment (Optional)"></textarea>
                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn pull-right">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        @endif

    </div>


@stop
@section('js')
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
    <script type="text/javascript">

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        // Add 0 to days and months
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }

        var today = dd + '/' + mm + '/' + yyyy;

        $('input[name="daterange"]').daterangepicker({

            locale: {
                format: 'Y/MM/DD HH:mm'
            },
            "minDate": today,
            "timePicker": true,
            "timePicker24Hour": true,
            "linkedCalendars": true,
            "timePickerIncrement": 15,
            "autoUpdateInput": true,
            "showCustomRangeLabel": false,
            "drops": "down"
        }, function (start, end, label) {
            console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
        });

        $('input[name="daterange"]').keydown(false);

    </script>
@stop

