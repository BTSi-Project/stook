<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            $event->menu->add('MAIN NAVIGATION');
            $event->menu->add([
                'text'        => 'Dashboard',
                'url'         => 'dashboard',
                'icon'        => 'dashboard',
            ]);

            if (Auth::check()) {
                $event->menu->add('ACCOUNT SETTINGS');
                $event->menu->add([
                    'text' => 'My Bookings',
                    'url'  => 'user/my-bookings',
                    'icon' => 'calendar',
                ]);

                if(Auth::user()->hasPermissionTo('view management')){
                    $event->menu->add('MANAGEMENT');
                    $event->menu->add([
                        'text' => 'Manage Users',
                        'url'  => 'users',
                        'icon' => 'user',
                        'can' => 'view users'
                    ]);
                    $event->menu->add([
                        'text' => 'Manage Roles',
                        'url'  => 'roles',
                        'icon' => 'users',
                        'can' => 'view roles'
                    ]);
                    $event->menu->add([
                        'text' => 'Manage Stock',
                        'url'  => 'categories',
                        'icon' => 'archive',
                        'can' => 'view categories'
                    ]);
                }
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
