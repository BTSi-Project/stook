<?php
/**
 * Created by PhpStorm.
 * User: marlo
 * Date: 14/03/2018
 * Time: 13:34
 */

namespace App;


class TextToImage
{
    private $img;


    function showImage()
    {
        header('Content-Type: image/png');
        return imagepng($this->img);
    }

    function saveAsPng($fileName = 'text-image', $location = '')
    {
        $fileName = $fileName . ".png";
        $fileName = !empty($location) ? $location . $fileName : $fileName;
        return imagepng($this->img, $fileName);
    }
}