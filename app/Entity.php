<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Entity extends Model
{
    public function status(){
        $currentTime = Carbon::now();
        $bookings = Booking::all()
            ->where(
                ['from','<',$currentTime],
                ['to','>',$currentTime]
            )->first();


        return $bookings == null;
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }
}
