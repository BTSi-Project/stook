<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Monolog\Handler\SyslogUdp\UdpSocket;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\User;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $roles = Role::all();
        return view("roles.index", compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        // Validate the data
        $this->validate($request, [
            'role-name' => 'required|max:255',
        ]);
        $role = new Role();
        $role->name = $request->input('role-name');
        $role->save();

        return redirect('roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  Role $role
     * @return Response
     */
    public function show(Role $role)
    {
        return view('roles.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Role $role
     * @return Response
     */
    public function edit(Role $role)
    {
        $permissions = Permission::all();
        $permissions_not_acquired = [];
        // Adds each permission that is not in the role.
        foreach ($permissions as $permission) {
            if (!$role->hasPermissionTo($permission)) {
                array_push($permissions_not_acquired, $permission);
            }
        }
        $users = User::all();
        $users_not_in_role = $users->diff($role->users);

        return view('roles.edit', compact(['role', 'permissions', 'permissions_not_acquired', 'users_not_in_role']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Role $role
     * @return Response
     */
    public function update(Role $role)
    {
        // Validate the data

        // Save it to the database

        // Redirect to the home page
        return redirect('roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $role
     * @return Response
     * @throws \Exception
     */
    public function destroy(Role $role)
    {
        $role->delete();
        return redirect('roles');
    }

    public function add(Role $role, Request $request)
    {
        $id = $request->input('id');
        $type = $request->input('type');

        if ($type === 'user') {
            $user = User::find($id);
            $user->assignRole($role);
        }
        if ($type === 'permission') {
            $permission = Permission::find($id);
            $role->givePermissionTo($permission);
        }
        return redirect('roles/' . $role->id . '/edit');
    }

    public function remove(Role $role, Request $request)
    {
        $id = $request->input('id');
        $type = $request->input('type');

        if ($type === 'user') {
            $user = User::find($id);
            $user->removeRole($role);
        }
        if ($type === 'permission') {
            $permission = Permission::find($id);
            $role->revokePermissionTo($permission);
        }
        return redirect('roles/' . $role->id . '/edit');
    }
}
