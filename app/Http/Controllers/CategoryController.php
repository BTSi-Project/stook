<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $categories = Category::all();
        return view('categories.index', compact('categories'));
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {
        // Validate the data
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $category = new Category();
        $category->name = $request->input('name');
        $category->save();
        return redirect('categories');
    }

    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }

    public function update()
    {

    }

    public function destroy(Category $category)
    {
        $category->delete();
        return redirect('categories');
    }
}
