<?php

namespace App\Http\Controllers;

use App\Entity;
use Illuminate\Http\Request;

class EntitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return string
     */
    public function store(Request $request)
    {
        // Validate the data
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $entity = new Entity();
        $entity->name = $request->input('name');
        $entity->category_id = $request->input('category_id');
        $entity->save();
        return redirect('categories/' . $request->input('category_id') . '/edit');

    }

    public function destroy(Entity $entity, Request $request)
    {
        $entity->delete();
        return redirect('categories/' . $request->input('category_id') . '/edit');
    }
}
