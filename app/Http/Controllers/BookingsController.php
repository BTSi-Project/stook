<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Category;
use App\Entity;
use App\TextToImage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class BookingsController extends Controller
{


    public function index()
    {
        $categories = Category::where('to', '>=', Carbon::now())->get();
        return view('bookings.index', compact('categories'));
    }

    public function myBookings()
    {
        $entriesPerPage = 10;

        $currentBookings = Booking::where('user_id', '=', Auth::user()->id)
            ->where('to', '>=', Carbon::now())
            ->paginate($entriesPerPage, ['*'], 'current_page');
        $pastBookings = Booking::where('user_id', '=', Auth::user()->id)
            ->where('to', '<', Carbon::now())
            ->paginate($entriesPerPage, ['*'], 'history_page');
        return view('user.myBookings', compact('currentBookings', 'pastBookings'));
    }

    public function create(Entity $entity)
    {
        // Fetch all Bookings from a specific Entity and exclude Bookings from the past
        $bookings = Booking::where('entity_id', $entity->id)
            ->whereDate('to', '>=', Carbon::now())
            ->orderBy('from')
            ->get();
        return view('bookings.create', compact('bookings', 'entity'));
    }

    public function store(Entity $entity)
    {
        $daterange = request('daterange');
        // Divide the dates and times
        $split = explode("-", $daterange);
        // String to time format
        $dates = array_map('strtotime', $split);
        // Create new Booking Object
        $booking = new Booking();
        $booking->from = date("Y-m-d H:i:s", $dates[0]);
        $booking->to = date("Y-m-d H:i:s", $dates[1]);
        $booking->comment = request('comment');
        $booking->user_id = Auth::user()->id;
        $booking->entity_id = $entity->id;
        // If the new dates don't overlap then create the new Object
        if ($this->isBookingPossible($booking)) {
            $booking->save();
        }
        return redirect('bookings/' . $entity->id . '/create');
    }

    public function image(Entity $entity)
    {

        $image = $this->generateImage($entity);
        $image = imagerotate($image, 0, 0);
        $bytes = $this->rawImage($image,false);
        $size = strlen($bytes);
        header("X-productionMode: true");
        header("X-sleepInterval: 60 ");
        header("Content-length: $size");
        header("Content-type: application/octet-stream");
        echo $bytes;
    }

    private function isBookingPossible(Booking $newBooking)
    {
        $bookings = Booking::all();
        foreach ($bookings as $booking) {
            // Check if it's the same entity
            if ($booking->entity->id === $newBooking->entity->id) {
                // Check if new booking's start date is inside a current booking
                if ($newBooking->from >= $booking->from && $newBooking->from <= $booking->to) {
                    return false;
                }
                // Check if new booking's end date is inside a current booking
                if ($newBooking->to > $booking->from && $newBooking->to < $booking->to) {
                    return false;
                }
                // Check if new booking's goes before and after the booking
                if ($newBooking->from < $booking->from && $newBooking->to > $booking->to) {
                    return false;
                }
            }
        }
        return true;
    }

    function rawImage($im, $hasRed)
    {
        $THRESHOLDS = array("black" => 150, "red" => 240);
        $bits = "";
        $bytes = "";
        $pixelcount = 0;
        for ($y = 0; $y < imagesy($im); $y++) {
            for ($x = 0; $x < imagesx($im); $x++) {
                $rgb = imagecolorat($im, $x, $y);
                $r = ($rgb >> 16) & 0xFF;
                $g = ($rgb >> 8) & 0xFF;
                $b = $rgb & 0xFF;
                $gray = ($r + $g + $b) / 3;
                if ($hasRed == "true") {
                    if (($r >= $THRESHOLDS['red']) && ($g < 50) && ($b < 50)) {
                        $bits .= "01";
                    } else {
                        if ($gray < $THRESHOLDS['black']) {
                            $bits .= "11";
                        } else {
                            $bits .= "00";
                        }
                    }
                    $pixelcount = $pixelcount + 2;
                } else {
                    if ($gray < $THRESHOLDS['black']) {
                        $bits .= "11";
                    } else {
                        $bits .= "00";
                    }
                    $pixelcount = $pixelcount + 2;
                }
                if ($pixelcount % 8 == 0) {
                    $bytes .= pack('H*', str_pad(base_convert($bits, 2, 16), 2, "0", STR_PAD_LEFT));
                    $bits = "";
                }
            }
        }
        $size = strlen($bytes);
        header("Content-length: $size");
        return $bytes;
    }

    public function generateImage(Entity $entity)
    {
        // Data
        $bookings = Booking::where('entity_id', $entity->id)
            ->whereDate('to', '>=', Carbon::now())
            ->orderBy('from')
            ->get();
        // Formatting
        $font = storage_path('app/arial.ttf');
        $imageWithd = 640;
        $imageHeight = 384;

        // Generate Images
        $image = imagecreate($imageWithd, $imageHeight);
        // Generate QR Code
        $qr = QrCode::format('png')->size(200)->generate(url('/') . '/bookings/' . $entity->id . '/create');

        // Colors
        $colorBlack = imagecolorallocate($image, 0, 0, 0);
        $colorWhite = imagecolorallocate($image, 255, 255, 255);

        // Set white background
        imagefilledrectangle($image, 0, 0, $imageWithd, $imageHeight, $colorWhite);

        // Header
        $headerString = $entity->category->name . ": " . $entity->name;
        imagettftext($image, 25, 0, 10, 40, $colorBlack, $font, $headerString);
        imageline($image, 0, 50, $imageWithd, 50, $colorBlack);

        // Reservations
        imagettftext($image, 16, 0, 10, 75, $colorBlack, $font, "Reservations");
        $yPosition = 110;
        if (count($bookings)) {
            foreach ($bookings as $booking) {
                $userText = $booking->user->first_name . " " . $booking->user->last_name;
                imagettftext($image, 14, 0, 10, $yPosition, $colorBlack, $font, $userText);
                $text = $booking->from . " - " . $booking->to;

                $yPosition += 20;
                imagettftext($image, 12, 0, 10, $yPosition, $colorBlack, $font, $text);
                $yPosition += 30;
            }
        } else {
            imagettftext($image, 12, 0, 10, $yPosition, $colorBlack, $font, "No reservations yet.");
        }

        // Copy and merge the QRCode Image
        imagealphablending($image, false);
        imagesavealpha($image, true);
        imagecopymerge($image, imagecreatefromstring($qr), 450, 200, 0, 0, 200, 200, 100);
        return $image;
    }


}
