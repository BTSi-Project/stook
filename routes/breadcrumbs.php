<?php

// Dashboard
Breadcrumbs::register('dashboard', function ($breadcrumbs) {
    $breadcrumbs->push("Dashboard", route('dashboard'));
});

// My Bookings
Breadcrumbs::register('myBookings', function ($breadcrumbs) {
    $breadcrumbs->push('My Bookings', route('user.myBookings'));
});

// Manage Users
Breadcrumbs::register('users', function ($breadcrumbs) {
    $breadcrumbs->push('Manager Users', route('users.index'));
});

// Home > Blog > [Category]
Breadcrumbs::register('category', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('blog');
    $breadcrumbs->push($category->title, route('category', $category->id));
});

// Home > Blog > [Category] > [Post]
Breadcrumbs::register('post', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('category', $post->category);
    $breadcrumbs->push($post->title, route('post', $post));
});