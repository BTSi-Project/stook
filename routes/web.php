<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Booking;
use App\Http\Resources\BookingsResource;

Auth::routes();

/**
 *  General
 */
Route::get('/', 'DashboardController@index');

Route::get('dashboard', 'DashboardController@index')->name('dashboard');


/**
 *  Management
 */

// Users
Route::group(['middleware' => ['permission:view users']], function () {
    // View
    Route::get('users', 'UsersController@index')->name('users.index');

    // Create
    Route::group(['middleware' => ['permission:create user']], function () {
        Route::post('users', 'UsersController@store')->name('users.store');
        Route::get('users/create', 'UsersController@create')->name('users.create');
    });

    // Update
    Route::group(['middleware' => ['permission:edit user']], function () {
        Route::get('users/{user}/edit', 'UsersController@edit')->name('users.edit');
        Route::put('users/{user}', 'UsersController@update')->name('users.update');
    });

    // Delete
    Route::group(['middleware' => ['permission:edit user']], function () {
        Route::delete('users/{user}', 'UsersController@destroy')->name('users.destroy');
    });

    // Approve
    Route::group(['middleware' => ['permission:approve user']], function () {
        Route::patch('users/{user}/approve', 'UsersController@approve')->name('users.approve');
    });

    // Block
    Route::group(['middleware' => ['permission:block user']], function () {
        Route::delete('users/{user}/fullDelete', 'UsersController@fullDelete')->name('users.fullDelete');
        Route::patch('users/{user}/restore', 'UsersController@restore')->name('users.restore');
    });
});
// Roles
Route::group(['middleware' => ['permission:view roles']], function () {
    // View
    Route::get('roles', 'RolesController@index')->name('roles.index');

    // Create
    Route::group(['middleware' => ['permission:create role']], function () {
        Route::post('roles', 'RolesController@store')->name('roles.store');
    });

    // Update
    Route::group(['middleware' => ['permission:edit role']], function () {
        Route::get('roles/{role}/edit', 'RolesController@edit')->name('roles.edit');
        Route::put('roles/{role}', 'RolesController@update')->name('roles.update');
        // Add relationship to role like users and permissions
        Route::post('roles/{role}/edit', 'RolesController@add')->name('roles.add');
        // Remove relationship to role like users and permissions
        Route::delete('roles/{role}/edit', 'RolesController@remove')->name('roles.remove');
    });

    // Delete
    Route::group(['middleware' => ['permission:delete role']], function () {
        Route::delete('roles/{role}', 'RolesController@destroy')->name('roles.destroy');
    });
});

// Categories
Route::group(['middleware' => ['permission:view categories']], function () {
    // View
    Route::get('categories', 'CategoryController@index')->name('categories.index');

    // Create
    Route::group(['middleware' => ['permission:create category']], function () {
        Route::post('categories', 'CategoryController@store')->name('categories.store');
    });

    // Update
    Route::group(['middleware' => ['permission:edit category']], function () {
        Route::get('categories/{category}/edit', 'CategoryController@edit')->name('categories.edit');
        Route::put('categories/{category}', 'CategoryController@update')->name('categories.update');
    });

    // Delete
    Route::group(['middleware' => ['permission:delete category']], function () {
        Route::delete('categories/{category}', 'CategoryController@destroy')->name('categories.destroy');
    });
});


// Entities
Route::group(['middleware' => ['permission:view entity']], function () {

    // Create
    Route::group(['middleware' => ['permission:create entity']], function () {
        Route::post('entities', 'EntitiesController@store')->name('entities.store');
    });

    // Delete
    Route::group(['middleware' => ['permission:edit entity']], function () {
        Route::delete('entities/{entity}', 'EntitiesController@destroy')->name('entities.destroy');
    });
});
Route::get('user/my-bookings', 'BookingsController@myBookings')->name('user.myBookings')->middleware('auth');

Route::get('bookings/{entity}/image', 'BookingsController@image')->name('bookings.image');
Route::get('bookings/{entity}/create', 'BookingsController@create')->name('bookings.create');
Route::POST('bookings/{entity}/create', 'BookingsController@store')->name('bookings.store')->middleware('auth');


Route::get('api/bookings/{}',function (){
    $booking = Booking::find(7)->first();
    return new BookingsResource($booking);
});
